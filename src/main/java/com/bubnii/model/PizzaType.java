package com.bubnii.model;

public enum PizzaType {
    CHEESE, VEGGIE, CLAM, PEPPERONI
}
