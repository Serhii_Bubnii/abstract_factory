package com.bubnii.model.factory;

import com.bubnii.model.PizzaType;
import com.bubnii.model.pizza.impl.DniproCheesePizza;
import com.bubnii.model.pizza.Pizza;
import com.bubnii.model.pizza.impl.DniproClamPizza;
import com.bubnii.model.pizza.impl.DniproPepperoniPizza;
import com.bubnii.model.pizza.impl.DniproVeggiePizza;

public class PizzeriaDnipro extends Pizzeria {
    @Override
    protected Pizza createPizza(PizzaType pizzaType) {
        switch (pizzaType) {
            case CHEESE:
                return new DniproCheesePizza();
            case VEGGIE:
                return new DniproVeggiePizza();
            case CLAM:
                return new DniproClamPizza();
            case PEPPERONI:
                return new DniproPepperoniPizza();
            default:
                throw new IllegalArgumentException("unknown " + pizzaType);
        }
    }
}
