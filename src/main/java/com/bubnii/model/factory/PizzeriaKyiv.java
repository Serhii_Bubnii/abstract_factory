package com.bubnii.model.factory;

import com.bubnii.model.PizzaType;
import com.bubnii.model.pizza.Pizza;
import com.bubnii.model.pizza.impl.KyivCheesePizza;
import com.bubnii.model.pizza.impl.KyivClamPizza;
import com.bubnii.model.pizza.impl.KyivPepperoniPizza;
import com.bubnii.model.pizza.impl.KyivVeggiePizza;

public class PizzeriaKyiv extends Pizzeria {
    @Override
    protected Pizza createPizza(PizzaType pizzaType) {
        switch (pizzaType) {
            case CHEESE:
                return new KyivCheesePizza();
            case VEGGIE:
                return new KyivVeggiePizza();
            case CLAM:
                return new KyivClamPizza();
            case PEPPERONI:
                return new KyivPepperoniPizza();
            default:
                throw new IllegalArgumentException("unknown " + pizzaType);
        }
    }
}
