package com.bubnii.model.factory;

import com.bubnii.model.PizzaType;
import com.bubnii.model.pizza.Pizza;
import com.bubnii.model.pizza.impl.LvivCheesePizza;
import com.bubnii.model.pizza.impl.LvivClamPizza;
import com.bubnii.model.pizza.impl.LvivPepperoniPizza;
import com.bubnii.model.pizza.impl.LvivVeggiePizza;

public class PizzeriaLviv extends Pizzeria {
    @Override
    protected Pizza createPizza(PizzaType pizzaType) {
        switch (pizzaType) {
            case CHEESE:
                return new LvivCheesePizza();
            case VEGGIE:
                return new LvivVeggiePizza();
            case CLAM:
                return new LvivClamPizza();
            case PEPPERONI:
                return new LvivPepperoniPizza();
            default:
                throw new IllegalArgumentException("unknown " + pizzaType);
        }
    }
}
