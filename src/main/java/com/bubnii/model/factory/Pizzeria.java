package com.bubnii.model.factory;

import com.bubnii.model.PizzaType;
import com.bubnii.model.pizza.Pizza;

public abstract class Pizzeria {

    protected abstract Pizza createPizza(PizzaType pizzaType);

    public Pizza ordering(PizzaType pizzaType) {
        Pizza pizza = createPizza(pizzaType);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }
}
