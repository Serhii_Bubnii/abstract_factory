package com.bubnii.model.pizza.impl;

import com.bubnii.model.pizza.Pizza;

public class DniproVeggiePizza extends Pizza {

    public DniproVeggiePizza(){
        name = "Dnipro veggie pizza";
        dough = "Thin crust dough";
        sauce = "Marinara sauce";

        toppings.add("Grated reggiano cheese");
        toppings.add("Garlic");
        toppings.add("Onion");
        toppings.add("Mushrooms");
    }
}
