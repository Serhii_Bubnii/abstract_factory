package com.bubnii.model.pizza.impl;

import com.bubnii.model.pizza.Pizza;

public class LvivPepperoniPizza extends Pizza {
    public LvivPepperoniPizza() {
        name = "Lviv pepperoni pizza";
        dough = "Extra thick crust dough";
        sauce = "Plum tomato sauce";

        toppings.add("Shredded dorbil cheese");
        toppings.add("Black olives");
        toppings.add("Spinach");
        toppings.add("Eggplant");
        toppings.add("Sliced pepperoni");
    }
}
