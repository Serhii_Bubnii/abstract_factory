package com.bubnii.model.pizza.impl;

import com.bubnii.model.pizza.Pizza;

public class KyivPepperoniPizza extends Pizza {

    public KyivPepperoniPizza() {
        name = "Kyiv pepperoni pizza";
        dough = "Thin crust dough";
        sauce = "Marinara sauce";

        toppings.add("Sliced pepperoni");
        toppings.add("Garlic");
        toppings.add("Mushrooms");
        toppings.add("Green pepper");
    }
}
