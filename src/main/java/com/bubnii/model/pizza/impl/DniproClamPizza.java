package com.bubnii.model.pizza.impl;

import com.bubnii.model.pizza.Pizza;

public class DniproClamPizza extends Pizza {
    public DniproClamPizza(){
        name = "Dnipro cheese pizza";
        dough = "Thin crust dough";
        sauce = "Marinara sauce";

        toppings.add("Fresh clams");
        toppings.add("Fresh squid");
    }
}
