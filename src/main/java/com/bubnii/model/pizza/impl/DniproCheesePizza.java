package com.bubnii.model.pizza.impl;

import com.bubnii.model.pizza.Pizza;

public class DniproCheesePizza extends Pizza {
    public DniproCheesePizza() {
        name = "Dnipro cheese pizza";
        dough = "Thin crust dough";
        sauce = "Marinara sauce";

        toppings.add("Grated cambotsola cheese");
    }
}
