package com.bubnii.model.pizza.impl;

import com.bubnii.model.pizza.Pizza;

public class LvivVeggiePizza extends Pizza {

    public LvivVeggiePizza() {
        name = "Lviv veggie pizza";
        dough = "Extra thick crust dough";
        sauce = "Plum tomato sauce";

        toppings.add("Shredded mozzarella cheese");
        toppings.add("Green olives");
        toppings.add("Spinach");
        toppings.add("Eggplant");
    }
}
