package com.bubnii.model.pizza.impl;

import com.bubnii.model.pizza.Pizza;

public class DniproPepperoniPizza extends Pizza {
    public DniproPepperoniPizza() {
        name = "Dnipro pepperoni pizza";
        dough = "Thin crust dough";
        sauce = "Marinara sauce";

        toppings.add("Sliced Pepperoni");
        toppings.add("Garlic");
        toppings.add("Onion");
        toppings.add("Mushrooms");
        toppings.add("Red pepper");
    }
}
