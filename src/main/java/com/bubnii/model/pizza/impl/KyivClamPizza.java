package com.bubnii.model.pizza.impl;

import com.bubnii.model.pizza.Pizza;

public class KyivClamPizza extends Pizza {

    public KyivClamPizza(){
        name = "Kyiv cheese pizza";
        dough = "Thin crust dough";
        sauce = "Marinara sauce";

        toppings.add("Fresh acipenser sturio");
        toppings.add("Fresh squalus acanthias");
    }
}
