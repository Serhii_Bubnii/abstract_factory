package com.bubnii.model.pizza.impl;

import com.bubnii.model.pizza.Pizza;

public class KyivVeggiePizza extends Pizza {

    public KyivVeggiePizza(){
        name = "Kyiv veggie pizza";
        dough = "Thin crust dough";
        sauce = "Marinara sauce";

        toppings.add("Grated gorgontsola cheese");
        toppings.add("Beans");
        toppings.add("Onion");
        toppings.add("Mushrooms");
    }
}
