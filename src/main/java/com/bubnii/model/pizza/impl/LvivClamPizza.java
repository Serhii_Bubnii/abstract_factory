package com.bubnii.model.pizza.impl;

import com.bubnii.model.pizza.Pizza;

public class LvivClamPizza extends Pizza {
    public LvivClamPizza(){
        name = "Lviv clam pizza";
        dough = "Extra thick crust dough";
        sauce = "Plum tomato sauce";

        toppings.add("Shredded cambotsola cheese");
        toppings.add("Frozen hypatis laeviuscula");
    }
}
