package com.bubnii.model.pizza.impl;

import com.bubnii.model.pizza.Pizza;

public class KyivCheesePizza extends Pizza {

    public KyivCheesePizza() {
        name = "Kyiv cheese pizza";
        dough = "Thin crust dough";
        sauce = "Marinara sauce";

        toppings.add("Grated mascarpone cheese");
    }
}
