package com.bubnii.model.pizza.impl;

import com.bubnii.model.pizza.Pizza;

public class LvivCheesePizza extends Pizza {

    public LvivCheesePizza() {
        name = "Lviv cheese pizza";
        dough = "Extra thick crust dough";
        sauce = "Plum tomato sauce";

        toppings.add("Shredded mozzarella cheese");
    }
}
