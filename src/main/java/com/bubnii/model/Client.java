package com.bubnii.model;

import com.bubnii.model.factory.Pizzeria;
import com.bubnii.model.factory.PizzeriaDnipro;
import com.bubnii.model.factory.PizzeriaKyiv;
import com.bubnii.model.factory.PizzeriaLviv;
import com.bubnii.model.pizza.Pizza;

public class Client {

    public static void main(String[] args) {
        Pizzeria pizzeriaLviv = new PizzeriaLviv();
        Pizzeria pizzeriaDnipro = new PizzeriaDnipro();
        Pizzeria pizzeriaKyiv = new PizzeriaKyiv();

        Pizza pizza = pizzeriaLviv.ordering(PizzaType.CHEESE);
        System.out.println("Joe ordered a " + pizza.getName());

        pizza = pizzeriaDnipro.ordering(PizzaType.CLAM);
        System.out.println("Bob ordered a " + pizza.getName());

        pizza = pizzeriaKyiv.ordering(PizzaType.PEPPERONI);
        System.out.println("Mark ordered a" + pizza.getName());

        pizza = pizzeriaLviv.ordering(PizzaType.PEPPERONI);
        System.out.println("Joe ordered a " + pizza.getName());

        pizza = pizzeriaDnipro.ordering(PizzaType.CHEESE);
        System.out.println("Bob ordered a " + pizza.getName());

        pizza = pizzeriaKyiv.ordering(PizzaType.VEGGIE);
        System.out.println("Mark ordered a" + pizza.getName());

        pizza = pizzeriaLviv.ordering(PizzaType.VEGGIE);
        System.out.println("Joe ordered a " + pizza.getName());

        pizza = pizzeriaDnipro.ordering(PizzaType.PEPPERONI);
        System.out.println("Bob ordered a " + pizza.getName());

        pizza = pizzeriaKyiv.ordering(PizzaType.CHEESE);
        System.out.println("Mark ordered a" + pizza.getName());
    }
}
